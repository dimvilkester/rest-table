
**Установка компонента**

1. Перенести компонент rest.table в папку local по следующему пути local/components/dv/rest.table
2. Перенести, либо скопировать правило обработки адресов из файла urlrewrite.php
3. Импортировать highloadblock из файла hblock_1_Offers.xml
4. Перенести папку api в корень проекта (в дальнейшем, обращение пойдет к ней)
5. В вызове компонента api/offers/index.php указать ИД highloadblock (В текущей настройке укзан `"HBLOCK_ID" => "1"`)


**Запросы к api**

Чтобы сделать запрос на получение/добавление/обновление данных необходимо пройти аутентификацию `Basic Auth`. Для этого вам необходимо ввести логин и пароль любого пользователя, который зарегистрирован в [административной панели](https://yadi.sk/i/64OHRzOYrvxi_Q) Битрикс. [Пример аутентификации](https://yadi.sk/i/BfMgmRG67exOlw).

*Список возможных запросов:*

1. `POST`. Добавление новой акции. [Пример](https://yadi.sk/i/mSAMkU-CDOJ3Mw).
	
	`http://domen.example/api/offers/`
	
	Набор полей при передаче:
		- title (string)
		- description (string)
		- tags (string)
		- picture (file)
		
2. `POST`. Обновление всех полей включая изображение. [Пример](https://yadi.sk/i/ZRn7ozbv0qxXZA).

	`http://domen.example/api/offers/{offer_id}/?_method=PUT` 
	
	Набор полей при передаче:
		- title (string)
		- description (string)
		- tags (string)
		- picture (file)
		
3. `PUT`. Обновление текстовых полей. [Пример](https://yadi.sk/i/AdqLiQ-whTSihQ).

    `http://domen.example/api/offers/{offer_id}`

	Для отправки новых данных необходимо сформировать json вида:

		```
        {
			"title": "Новый заголовок",
			"description": "Новое описание",
			"tags": "tags"
		}
        ```

		
4. `GET`. Получение списка акций `http://domen.example/api/offers/`. [Пример](https://yadi.sk/i/pH_plzo6u2Po-A).

5. `GET`. Получение конкретной акции `http://domen.example/api/offers/{offer_id}/`. [Пример](https://yadi.sk/i/OXzhhCEq0QKzNg).


**Логирование**

Папка log будет автоматически создана в корне сайта `log/rest_table.log`

