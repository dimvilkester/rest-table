<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    'NAME' => 'Компонент для CRUD операций',
    'DESCRIPTION' => 'Компонент для CRUD операций',
    'SORT' => 10,
    'CACHE_PATH' => 'Y',
    'PATH' => array(
        'ID' => 'dv',
        'NAME' => 'dv',
    ),
);