<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Application,
    \Bitrix\Main\Data\Cache,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock\HighloadBlockTable;

class CRestTable extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams) {

        if (! isset($arParams['FILTER'])) {
            $arParams['FILTER'] = array();
        }

        $arParams['HBLOCK_ID'] = (int)$arParams['HBLOCK_ID'];

        return $arParams;
    }

    public function controller() {

        global $DB;

        $request = Application::getInstance()->getContext()->getRequest();

        $result = array();
        $dataRequest = array();

        $elementId = (int)$request->get('ID');

        switch ($request->getRequestMethod()) {
            case 'GET':

                if ($elementId > 0) {

                    $result = $this->getById($elementId);
                } else {

                    $result = $this->getList();
                }

                break;
            case 'POST':

                $dataRequest = $request->getPostList()->toArray();

                if ($elementId > 0 && $request->get('_method') === 'PUT') {

                    $result = $this->updateFormData($elementId, $dataRequest);

                } else {

                    $result = $this->add($dataRequest);
                }

                break;
            case 'PUT':

                if ($elementId > 0) {

                    $dataRequest = file_get_contents('php://input');
                    $dataRequest = json_decode($dataRequest, true);

                    $result = $this->updateRaw($elementId, $dataRequest);
                }

                break;

            case 'DELETE':

                if ($elementId > 0) {

                    $result = $this->delete($elementId);
                }

                break;
        }

        $arRequestLog = $dataRequest;
        if (count($dataRequest) > 0) {
            $arRequestLog = $dataRequest[0];
        }

        if ($arFile = $request->getFile('picture')) {
            $arRequestLog['picture'] = $arFile;
        }

        $arLog = array(
            'date' => date($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), time()),
            'request_uri' => $request->getRequestUri(),
            'method' => $request->getRequestMethod(),
            '_method_ext' => $request->get('_method'),
            'request' => $arRequestLog,
            'response' => $result
        );

        \Bitrix\Main\IO\Directory::createDirectory($_SERVER['DOCUMENT_ROOT'].'/log/');
        \Bitrix\Main\Diag\Debug::writeToFile($arLog, '', '/log/rest_table.log');

        return $result;

    }

    private function add($dataRequest = array()) {

        $HLBlock = $this->getHLBlock();

        if ($HLBlock !== '' && ! empty($dataRequest)) {

            $arElementFields = array();
            foreach ($dataRequest as $codeRequest => $valueRequest) {
                $userFiledName = 'UF_'.mb_strtoupper($codeRequest);

                $arElementFields[$userFiledName] = htmlspecialchars($valueRequest);
            }

            if ($arImageFile = $this->uploadImage()) {
                $arElementFields['UF_PICTURE'] = $arImageFile;
            }

            $isSuccess = false;
            if (! empty($arElementFields)) {

                $obResult = $HLBlock::add($arElementFields);
                $elementId = $obResult->getID();
                $isSuccess = $obResult->isSuccess();
            }

            if ($elementId && $isSuccess) {

                \CHTTP::SetStatus('201 Created');

                $result = array(
                    'status' => '201',
                    'message' => 'Element added successfully',
                    'id' => $elementId
                );
            } else {

                \CHTTP::SetStatus('400 Bad Request');

                $result = array(
                    'status' => '400',
                    'message' => 'Bad Request'
                );
            }

        } else {

            \CHTTP::SetStatus('400 Bad Request');

            $result = array(
                'status' => '400',
                'message' => 'Bad Request'
            );
        }

        return $result;
    }

    private function getList() {

        $HLBlock = $this->getHLBlock();

        $arElements = array();

        if ($HLBlock !== '') {

            $rsElements = $HLBlock::getList(array(
                //'filter' => array(),
                'select' => array(
                    'ID',
                    'UF_*'
                ),
                'order' => array(
                    'ID' => 'ASC'
                ),
                'limit' => '50',
            ));
            while ($arElement = $rsElements->Fetch()) {

                $arElement['UF_PICTURE'] = \CFile::GetPath($arElement['UF_PICTURE']);
                $arElements[] = $arElement;
            }
            unset($rsElements);

            if (! empty($arElements)) {

                $result = $arElements;
                unset($arElements);
            } else {

                \CHTTP::SetStatus('404 Not Found');

                $result = array(
                    'status' => '404',
                    'message' => 'Elements not found'
                );
            }

        } else {

            \CHTTP::SetStatus('404 Not Found');

            $result = array(
                'status' => '404',
                'message' => 'Elements not found'
            );
        }

        return $result;
    }

    private function getById($elementId) {

        $HLBlock = $this->getHLBlock();
        $elementId = (int)$elementId;

        if ($HLBlock !== '' && $elementId > 0) {

            if ($arElement = $HLBlock::getById($elementId)->Fetch()) {

                $arElement['UF_PICTURE'] = \CFile::GetPath($arElement['UF_PICTURE']);

                $result = $arElement;
                unset($arElement);
            } else {

                \CHTTP::SetStatus('404 Not Found');

                $result = array(
                    'status' => '404',
                    'message' => 'Element not found'
                );
            }
        } else {

            \CHTTP::SetStatus('404 Not Found');

            $result = array(
                'status' => '404',
                'message' => 'Element not found'
            );
        }

        return $result;
    }

    private function updateFormData($elementId, $dataRequest = array()) {

        $HLBlock = $this->getHLBlock();
        $elementId = (int)$elementId;

        if ($HLBlock !== ''
            && $elementId > 0
        ) {
            $arElement = $HLBlock::getById($elementId)->Fetch();
        }

        if ($HLBlock !== ''
            && $arElement['ID'] > 0
        ) {

            $arElementFields = array();
            foreach ($dataRequest as $codeRequest => $valueRequest) {
                $userFiledName = 'UF_'.mb_strtoupper($codeRequest);

                $arElementFields[$userFiledName] = htmlspecialchars($valueRequest);
            }

            if ($arImageFile = $this->uploadImage($arElement['UF_PICTURE'])) {
                $arElementFields['UF_PICTURE'] = $arImageFile;
            }

            $isSuccess = false;
            if (! empty($arElementFields)) {

                $obResult = $HLBlock::update($elementId, $arElementFields);
                $isSuccess = $obResult->isSuccess();
            }

            if ($isSuccess) {

                \CHTTP::SetStatus('200 OK');

                $result = array(
                    'status' => '200',
                    'message' => 'Element updated successfully',
                );
            } else {

                \CHTTP::SetStatus('400 Bad Request');

                $result = array(
                    'status' => '400',
                    'message' => 'Bad Request'
                );
            }

        } else {

            \CHTTP::SetStatus('404 Not Found');

            $result = array(
                'status' => '404',
                'message' => 'Element not found'
            );
        }

        return $result;
    }

    private function updateRaw($elementId, $dataRequest = array()) {


        $HLBlock = $this->getHLBlock();
        $elementId = (int)$elementId;

        if ($HLBlock !== ''
            && $elementId > 0
        ) {
            $arElement = $HLBlock::getById($elementId)->Fetch();
        }

        if ($HLBlock !== ''
            && ! empty($dataRequest)
            && $arElement['ID'] > 0
        ) {

            $arElementFields = array();
            foreach ($dataRequest as $codeRequest => $valueRequest) {
                $userFiledName = 'UF_'.mb_strtoupper($codeRequest);

                $arElementFields[$userFiledName] = htmlspecialchars($valueRequest);
            }

            $obResult = $HLBlock::update($elementId, $arElementFields);
            $isSuccess = $obResult->isSuccess();

            if ($isSuccess) {

                \CHTTP::SetStatus('200 OK');

                $result = array(
                    'status' => '200',
                    'message' => 'Element updated successfully',
                );
            } else {

                \CHTTP::SetStatus('400 Bad Request');

                $result = array(
                    'status' => '400',
                    'message' => 'Bad Request'
                );
            }

        } else {

            \CHTTP::SetStatus('404 Not Found');

            $result = array(
                'status' => '404',
                'message' => 'Element not found'
            );
        }

        return $result;
    }

    private function delete($elementId) {

        $HLBlock = $this->getHLBlock();
        $elementId = (int)$elementId;

        if ($HLBlock !== ''
            && $elementId > 0
        ) {
            $arElement = $HLBlock::getById($elementId)->Fetch();
        }

        if ($HLBlock !== ''
            && $arElement['ID'] > 0
        ) {

            $obResult = $HLBlock::delete($elementId);
            $isSuccess = $obResult->isSuccess();

            if ($isSuccess) {

                \CHTTP::SetStatus('200 OK');

                $result = array(
                    'status' => '200',
                    'message' => 'Element deleted successfully',
                );
            } else {

                \CHTTP::SetStatus('400 Bad Request');

                $result = array(
                    'status' => '400',
                    'message' => 'Bad Request'
                );
            }

        } else {

            \CHTTP::SetStatus('404 Not Found');

            $result = array(
                'status' => '404',
                'message' => 'Element not found'
            );
        }

        return $result;
    }

    private function getHLBlock() {

        $HLBlock = '';

        if (Loader::includeModule('highloadblock')) {

            $HLBlockId = $this->arParams['HBLOCK_ID'];
            $arHLBlock = HighloadBlockTable::getById($HLBlockId)->fetch();
            if ($HLBlockEntity = HighloadBlockTable::compileEntity($arHLBlock)) {
                $HLBlock = $HLBlockEntity->getDataClass();
            }
        }

        return $HLBlock;
    }

    private function uploadImage($oldImageId = null) {

        $request = Application::getInstance()->getContext()->getRequest();

        $requestImageFile = $request->getFile('picture');

        $oldImageId = (int)$oldImageId;
        $imageMaxFileSize = 2000000;

        $arImageFile = array(
            'name' => \CUtil::ConvertToLangCharset($requestImageFile['name']),
            'size' => $requestImageFile['size'],
            'tmp_name' => $requestImageFile['tmp_name'],
            'type' => $requestImageFile['type'],
            'MODULE_ID' => 'main',
        );

        if ($oldImageId > 0) {
            $arImageFile['old_file'] = $oldImageId;
            $arImageFile['del'] = 'Y';
        }

        $rsImageFile = \CFile::CheckImageFile($arImageFile, $imageMaxFileSize, 0, 0);

        if (strlen($rsImageFile) <= 0) {
            return $arImageFile;
        } else {
            return false;
        }

    }

    private function isUserPassword($userLogin = '', $password = '') {

        $result = false;

        $rsUser = \Bitrix\Main\UserTable::getList(
            array(
                'select' => array(
                    'ID',
                    'LOGIN',
                    'PASSWORD'
                ),
                'filter' => array(
                    'LOGIN' => $userLogin
                )
            )
        );
        if ($arUser = $rsUser->fetch()) {

            $salt = substr($arUser['PASSWORD'], 0, (strlen($arUser['PASSWORD']) - 32));

            $realPassword = substr($arUser['PASSWORD'], -32);
            $password = md5($salt . $password);

            $result = ($password === $realPassword);
        }

        return $result;
    }

    public function executeComponent() {

        $server = Application::getInstance()->getContext()->getServer();
        $authUser = $server->get('PHP_AUTH_USER');

        if (! isset($authUser)) {

            \CHTTP::SetAuthHeader();

            die('Sorry, you need proper credentials!');
        } else {

            $arAuthRequest = \CHTTP::ParseAuthRequest();

            if ($this->isUserPassword($arAuthRequest['basic']['username'], $arAuthRequest['basic']['password'])) {
                $this->arResult = $this->controller();
                $this->arResult = json_encode($this->arResult, JSON_UNESCAPED_UNICODE || JSON_NUMERIC_CHECK);

                header('Access-Control-Allow-Origin: *');
                header('Access-Control-Allow-Headers: *');
                header('Access-Control-Allow-Methods: *');
                header('Access-Control-Allow-Credentials: true');
                header('Content-type: application/json');

                echo $this->arResult;
            } else {

                \CHTTP::SetAuthHeader();

                die('Sorry, incorrect data entered!');
            }

        }

    }

}